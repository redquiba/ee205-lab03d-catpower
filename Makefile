###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 03d - Cat Power - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### 
###
### @author Ryan Edquiba 
### @date   
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = catPower

all: $(TARGET)

crazyCatGuy: catPower.cpp
	$(CC) $(CFLAGS) -o $(TARGET) catPower.cpp

clean:
	rm -f $(TARGET) *.o

